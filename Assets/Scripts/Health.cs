using UnityEngine;

public class Health : MonoBehaviour {
    private GameManager gameManager;
    private Vector3 startPosition;

    private void Awake() {
        gameManager = FindObjectOfType<GameManager>();
        if(gameManager == null) Debug.Log("� �� ����� GameManager");
    }

    private void Start() {
        startPosition = transform.position;
    }

    public void TakeDamage() {
        gameManager.LoseLife();
        transform.position = startPosition;
    }
}