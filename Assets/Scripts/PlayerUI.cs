using UnityEngine.UI;
using UnityEngine;

public class PlayerUI : MonoBehaviour {
    public static PlayerUI ui;
    [SerializeField] private Text livesText;
    [SerializeField] private Text coinText;
    private int coinCount;

    private void Awake() {
        if(ui == null) ui = this;
        else Destroy(this);
    }

    public void SetLives(int amount) {
        livesText.text = amount.ToString();
    }

    public void ShowCointCount(int amount) {
        coinText.text = amount.ToString();
    }
}