using UnityEngine;

public class Coin : MonoBehaviour {
    [SerializeField] private string playerLayer = "Player";
    [SerializeField] private int coinCost = 1;
    private bool isCollected;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.layer != LayerMask.NameToLayer(playerLayer)) return;
        PickCoin();
    }

    private void PickCoin() {
        if(isCollected) return;
        isCollected = true;
        GameManager gameManager = FindObjectOfType<GameManager>();
        if(gameManager != null) gameManager.AddCoin(coinCost);
        else Debug.Log("�� ����� Game");
        Destroy(gameObject);
    }
}