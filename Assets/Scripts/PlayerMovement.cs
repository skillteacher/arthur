using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    [SerializeField] private string groundLayerName = "Ground";
    [SerializeField] public float speed = 10f;
    [SerializeField] private bool isSpriteTurnRight = true;
    private KeyCode jumpAxisName = KeyCode.Space;
    public float jumpForce = 10f;
    private Rigidbody2D playerRigidbody;
    private bool isGrounded = true;

    void Awake() {
        playerRigidbody = GetComponent<Rigidbody2D>();
    }

    void Update() {
        float horizntalInput = Input.GetAxis("Horizontal");
        if (Input.GetKeyDown(jumpAxisName) && isGrounded) Jump();
        Move(horizntalInput);
        Flip(horizntalInput);

    }

    private void Move(float direction) {
        Vector2 velocity = new Vector2(speed * direction, playerRigidbody.velocity.y);
        playerRigidbody.velocity = velocity;
    }   

    private void Jump() {
        isGrounded = false;
        Vector2 jumpVector = new Vector2(0f, jumpForce);
        playerRigidbody.velocity += jumpVector;
    }

    private void Flip(float horizontalInput) {
        if (horizontalInput == 0f) return;
        bool direction = horizontalInput > 0f;
        Vector3 scale = transform.localScale;
        if (direction != isSpriteTurnRight) {
            transform.localScale = new Vector3(scale.x * -1f, scale.y, scale.z);
            isSpriteTurnRight = direction;
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(LayerMask.NameToLayer(groundLayerName) != other.gameObject.layer) return;
        isGrounded = true;
    }
}