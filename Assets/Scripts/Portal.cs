using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour {
    [SerializeField] private string playerLayerName = "Player";
    [SerializeField] private string nextLevelName;

    private void OnTriggerEnter2D(Collider2D collision) {
        if(LayerMask.NameToLayer(playerLayerName) != collision.gameObject.layer) return;
        SceneManager.LoadScene(nextLevelName);
    }
}