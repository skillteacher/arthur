using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleEnemy : MonoBehaviour {
    [SerializeField] private string groundLayerName = "Ground";
    [SerializeField] private float speed = 10f;
    private Rigidbody2D enemyRigidbody;
    private int groundLayerNumber;
    private bool isTurnedRight;

    public void Awake() {
        groundLayerNumber = LayerMask.NameToLayer(groundLayerName);
        enemyRigidbody = GetComponent<Rigidbody2D>();
        isTurnedRight = transform.localScale.x > 0;
    }

    private void Update() {
        Vector2 velocity = new Vector2(isTurnedRight ? speed : -speed, enemyRigidbody.velocity.y);
        enemyRigidbody.velocity = velocity;
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (groundLayerNumber != collision.gameObject.layer) return;
 
        Vector3 scale = transform.localScale;
        transform.localScale = new Vector3(-scale.x, scale.y, scale.z);
        isTurnedRight = !isTurnedRight;
    }

    public void Flip() {
        isTurnedRight = !isTurnedRight;
        transform.Rotate(0f, 180f, 0f);
    }
}